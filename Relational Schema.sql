
/*Hej Niels. Jeg er ikke sikker på om jeg har gjort det rigtigt, men jeg har forsøget.*/

-- MySQL dump 10.9
--
-- Host: localhost    Database: world
-- ------------------------------------------------------
-- Server version	4.1.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

create database test1

use test1

--
-- Table structure for table city
--

DROP TABLE IF EXISTS city;
CREATE TABLE city (
  id int(11) NOT NULL auto_increment,
  name char(35) NOT NULL default '',
  countryCode char(3) NOT NULL default '',
  district char(20) NOT NULL default '',
  population int(11) NOT NULL default '0',
  PRIMARY KEY  (ID),
  foreign key (countrycode) references country(code)
) ENGINE=innodb DEFAULT CHARSET=latin1;

--
-- Dumping data for table city
--

INSERT INTO city VALUES (3315,'København','DNK','København',495699);
INSERT INTO city VALUES (1606,'Skrydstrup','DNK','Skrydstrup',?);
INSERT INTO city VALUES (1609,'Vojens','DNK','Vojens',?);
INSERT INTO city VALUES (2896,'Haderslev','DNK','Haderslev',?);

--
-- Table structure for table country
--

DROP TABLE IF EXISTS country;
CREATE TABLE country (
  code char(3) NOT NULL default '',
  name char(52) NOT NULL default '',
  continent enum('Asia','Europe','North America','Africa','Oceania','Antarctica','South America') NOT NULL default 'Asia',
  region char(26) NOT NULL default '',
  surfacearea float(10,2) NOT NULL default '0.00',
  indepyear smallint(6) default NULL,
  population int(11) NOT NULL default '0',
  lifeexpectancy float(3,1) default NULL,
  gnp float(10,2) default NULL,
  gnpold float(10,2) default NULL,
  localname char(45) NOT NULL default '',
  governmentform char(45) NOT NULL default '',
  headofstate char(60) default NULL,
  capital int(11) default NULL,
  code2 char(2) NOT NULL default '',
  PRIMARY KEY  (Code)
) ENGINE=innodb DEFAULT CHARSET=latin1;

--
-- Dumping data for table country
--

INSERT INTO country VALUES ('DNK','Denmark','Europe','Nordic Countries',43094.00,800,5330000,76.5,174099.00,169264.00,'Danmark','Constitutional Monarchy','Margrethe II',3315,'DK');

--
-- Table structure for table countrylanguage
--

DROP TABLE IF EXISTS countrylanguage;
CREATE TABLE countrylanguage (
  countrycode char(3) NOT NULL default '',
  language char(30) NOT NULL default '',
  isofficial enum('T','F') NOT NULL default 'F',
  percentage float(4,1) NOT NULL default '0.0',
  PRIMARY KEY  (CountryCode,Language),
  foreign key(countrycode) references country (code)
) ENGINE=innodb DEFAULT CHARSET=latin1;

--
-- Dumping data for table countrylanguage
--

INSERT INTO countrylanguage VALUES ('DNK','Danish','T',93.5);
INSERT INTO countrylanguage VALUES ('DNK','English','F',0.3);
